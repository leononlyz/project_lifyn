import 'package:flutter/material.dart';
import 'package:project_l/util/screenAwareSize.dart';
import './src/services/api/AuthApi.dart';
import './src/widgets/signUpForm.dart';
import './src/widgets/loginForm.dart';
import './src/screens/AuthScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lifyn Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: Colors.pinkAccent,
      ),
      // routes: {

      //     '/': (BuildContext context) => AuthScreen()
      // },
      home: AuthScreen(
        key: GlobalKey(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          title: Text('Lifyn'),
        ),
        body: ListView(
          children: <Widget>[
            Center(
              // color: Colors.red,
              // height: MediaQuery.of(context).size.height / 3.5,
              // screenAwareSize(165, context),
              child: signInScreen(context),
              // ListView(
              //     scrollDirection: Axis.horizontal,
              //     padding: EdgeInsets.only(left: 10.0),
              //     children: <Widget>[
              //       Row(children: <Widget>[
              //         colorCard("dsdsdsdsds", 12.00, 1, context, Colors.cyan),
              //         colorCard("gggg", 12.00, 0, context, Colors.green),
              //         colorCard("dsdsdsdsds", 12.00, 1, context, Colors.cyan),
              //         colorCard("gggg", 12.00, 0, context, Colors.black),
              //         colorCard("dsdsdsdsds", 12.00, 1, context, Colors.purple),
              //         colorCard("gggg", 12.00, 0, context, Colors.yellow),
              //       ]),
              //     ]),
            ),
          ],
        )

        // Align(
        //   alignment: Alignment.centerLeft,
        //   child: Container(
        //       padding: EdgeInsets.only(left: 15.0),
        //       child: Text('Erteer',
        //           style: TextStyle(
        //               fontSize: 24.0, fontWeight: FontWeight.bold))),
        // ),
        // Expanded(
        //   child: ListView(
        //       padding: EdgeInsets.only(left: 10.0, bottom: 20.0),
        //       children: <Widget>[
        //         Column(children: <Widget>[
        //           colorCard(
        //               "dsdsdsdsds", 12.00, 1, context, Colors.cyan, true),
        //           colorCard("gggg", 12.00, 0, context, Colors.green, true),
        //           colorCard(
        //               "dsdsdsdsds", 12.00, 1, context, Colors.cyan, true),
        //           colorCard("gggg", 12.00, 0, context, Colors.black, true),
        //           colorCard(
        //               "dsdsdsdsds", 12.00, 1, context, Colors.purple, true),
        //           colorCard("gggg", 12.00, 0, context, Colors.yellow, true),
        //         ]),
        //       ]),
        // )
        );
  }
}

Widget colorCard(
    String text, double amount, int type, BuildContext context, Color color,
    [bool full = false]) {
  final _media = MediaQuery.of(context).size;
  return Container(
    margin: EdgeInsets.only(top: 15, right: 15),
    padding: EdgeInsets.all(15),
    height: screenAwareSize(90, context),
    width: !full ? _media.width / 2 - 25 : _media.width,
    decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
              color: color.withOpacity(0.4),
              blurRadius: 16,
              spreadRadius: 0.2,
              offset: Offset(0, 8)),
        ]),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(
          "${type > 0 ? "" : "-"} \$ ${amount.toString()}",
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    ),
  );
}

Widget profileCard(String text, BuildContext context, Color color,
    [bool full = false]) {
  final _media = MediaQuery.of(context).size;
  return Container(
      margin: EdgeInsets.only(
          top: screenAwareSize(10, context),
          right: screenAwareSize(10, context),
          left: screenAwareSize(10, context)),
      //  EdgeInsets.only(top: 15, right: 15, left: 15),
      padding: EdgeInsets.only(
          left: screenAwareSize(15, context),
          top: screenAwareSize(10, context),
          bottom: screenAwareSize(10, context)),
      // height: screenAwareSize(400, context),
      width: _media.width,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
                color: color.withOpacity(0.4),
                blurRadius: 16,
                spreadRadius: 0.2,
                offset: Offset(0, 8)),
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
              child: CircleAvatar(
            radius: 50.0,
            backgroundColor: Colors.white,
            child: Image.network(
              "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
              height: screenAwareSize(60, context),
              width: screenAwareSize(60, context),
              colorBlendMode: BlendMode.darken,
            ),
          )),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: _media.width / 2,
                alignment: Alignment.centerRight,
                child: Text('Owner', style: TextStyle(color: Colors.white)),
              ),
              Container(
                  width: _media.width / 2,
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        text,
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'Rating : 5/10',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  )),
              Container(
                margin: EdgeInsets.only(left: 3),
                width: _media.width / 2,
                alignment: Alignment.bottomRight,
                child: RaisedButton(
                    color: Colors.cyan,
                    splashColor: Colors.blue,
                    // highlightColor: Colors.green,
                    onPressed: () {
                      print("onTap called.");
                    },
                    child: Text(
                      "Settings",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
              )
            ],
          ),
        ],
      ));
}

Widget signInScreen(BuildContext context) {
  final _media = MediaQuery.of(context).size;
  bool signInBool = false;
  bool signUpBool = true;
  return (Container(
      width: _media.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // Container(
          //     child: CircleAvatar(
          //   radius: 75.0,
          //   backgroundColor: Colors.orange,
          //   child: CircleAvatar(
          //     radius: 70.0,
          //     backgroundColor: Colors.white70,
          //     child: Image.network(
          //       "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png",
          //       height: screenAwareSize(85, context),
          //       width: screenAwareSize(85, context),
          //       colorBlendMode: BlendMode.darken,
          //     ),
          //   ),
          // )),
          SizedBox(height: 30.0),
          !signUpBool && !signInBool
              ? Column(
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Welcome home, Sky.",
                        style: TextStyle(
                          fontSize: 22,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Not You? ",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            print('sign in');
                          },
                          child: Text(
                            "Sign In here",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.orange,
                            ),
                          ),
                        )
                      ],
                    )),
                  ],
                )
              : signInBool
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Sign In",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 40.0),
                          child: LoginForm(),
                        )
                      ],
                    )
                  : Column(
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 40.0),
                          child: SignUpForm(),
                        )
                      ],
                    )
        ],
      )));
}
