import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
import 'dart:convert';
// import '../models/item_model.dart';
import '../../models/user.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:equatable/equatable.dart';

class AuthApi {
  Dio client = Dio();
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final facebookLogin = FacebookLogin();
  // final _apiKey = '46d7730020338f57fedd8ffdd0cf414d';

  // Future<ItemModel> fetchMovieList() async {
  //   print("entered");
  //   final response = await client
  //       .get("http://api.themoviedb.org/3/movie/popular?api_key=$_apiKey");
  //   print(response.body.toString());
  //   if (response.statusCode == 200) {
  //     // If the call to the server was successful, parse the JSON
  //     return ItemModel.fromJson(json.decode(response.body));
  //   } else {
  //     // If that call was not successful, throw an error.
  //     throw Exception('Failed to load post');
  //   }
  // }

  Future<Map> signUp(Map param) async {
    try {
      final response = await client.post(
        'http://68.183.184.147:3006/user/signup',
        data: param,
        // options: new Options(
        //     contentType: ContentType.parse("application/x-www-form-urlencoded")),
      );

      if (response.statusCode == 200) {
        Map res = response.data;
        print(response.data);
        return res;
        // print(UserModel);
        // return UserModel.fromMap(json.decode(response.data));
      } else {
        Map res = response.data;

        return res;
      }
    } catch (e) {
      print(e.response.data['message']);
      return e.response.data;
    }
  }

  Future<Map> signIn(Map param) async {
    try {
      final response = await client.post(
        'http://68.183.184.147:3006/user/login',
        data: param,
        // options: new Options(
        //     contentType: ContentType.parse("application/x-www-form-urlencoded")),
      );

      if (response.statusCode == 200) {
        Map res = response.data;
        print(response.data);
        return res;
        // print(UserModel);
        // return UserModel.fromMap(json.decode(response.data));
      } else {
        Map res = response.data;

        return res;
      }
    } catch (e) {
      print(e.response.data['message']);
      return e.response.data;
    }
  }

  Future<Map> googleSignIn() async {
    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
      GoogleSignInAuthentication googleAuth =
          await googleSignInAccount.authentication;

      print(googleSignInAccount);
      print(googleAuth);

      final Map param = {'type': 'gm', 'gm_id': googleAuth.accessToken};

      Map response = await signUp(param);

      return response;
      // GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    } catch (err) {
      print(err);
      final Map error = {'sucess': false, 'message': err};
      return error;
    }
  }

  Future<Map> facebookSignIn() async {
    var facebookLoginResult =
        await facebookLogin.logInWithReadPermissions(['email']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print(FacebookLoginStatus.error);
        final Map error = {
          'sucess': false,
          'message': FacebookLoginStatus.error
        };
        return error;
        break;
      case FacebookLoginStatus.cancelledByUser:
        print(FacebookLoginStatus.cancelledByUser);
        final Map error = {'sucess': false, 'message': "User cancelled."};
        return error;
        break;
      case FacebookLoginStatus.loggedIn:

        // call sign up (facebook type) api

        //return data then store in model
        // var graphResponse = await http.get(
        //     'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult
        //         .accessToken.token}');

        // var profile = json.decode(graphResponse.body);
        print(FacebookLoginStatus);
        print(facebookLoginResult.accessToken.token);

        final Map param = {
          'type': 'fb',
          'fb_id': facebookLoginResult.accessToken.token
        };

        Map response = await signUp(param);
        return response;
        // onLoginStatusChanged(true, profileData: profile);
        break;
    }
  }
}
