import 'package:flutter/material.dart';
import '../widgets/loginForm.dart';
import '../widgets/signUpForm.dart';

class AuthScreen extends StatefulWidget {
  final Widget child;

  AuthScreen({Key key, this.child}) : super(key: key);

  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lifyn'),
      ),
      body: LoginForm(),
    );
  }
}
