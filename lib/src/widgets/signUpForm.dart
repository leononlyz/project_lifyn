import '../services/api/AuthApi.dart';
import 'package:flutter/material.dart';

class SignUpForm extends StatefulWidget {
  @override
  SignUpFormState createState() {
    return SignUpFormState();
  }
}

// Create a corresponding State class. This class will hold the data related to
// the form.
class SignUpFormState extends State<SignUpForm> {
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a GlobalKey<FormState>, not a GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPassController = TextEditingController();
  final nameController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  bool dateSelected = false;
  bool _autoValidate = false;

  bool _isLoading = false;
  bool _isGoogleLoading = false;
  bool _isFbLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree

    super.dispose();
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  String _validateName(String value) {
    if (value.length > 8) {
      return null;
    }
    return "Name must be 8 characters and above.";
  }

  String _validatePassword(String value) {
    if (value.length > 7) {
      return null;
    }
    return 'Password must be upto 8 characters';
  }

  String _validateConfirmPassword(String value) {
    if (value == passwordController.text && confirmPassController.text != "") {
      return null;
    }
    return 'Password not match.';
  }

  void _signUp(BuildContext context) async {
    // Validate will return true if the form is valid, or false if
    // the form is invalid.
    setState(() {
      _isLoading = true;
    });
    if (_formKey.currentState.validate()) {
      // If the form is valid, we want to show a Snackbar

      Map param = {
        'name': nameController.text,
        'email': emailController.text,
        'password': passwordController.text,
        'type': 'plain'
      };

      if (dateSelected == true) {
        param['dob'] =
            "${selectedDate.toLocal().year}-${selectedDate.toLocal().month}-${selectedDate.toLocal().day}";
      }

      final Map response = await AuthApi().signUp(param);

      if (response['sucess'] == true) {
        setState(() {
          _isLoading = false;
        });
        print(response);
      } else {
        setState(() {
          _isLoading = false;
        });
        print(response);
      }
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(response['message'])));
    } else {
      setState(() {
        _autoValidate = true; // activate auto validator after first attempt
        _isLoading = false;
      });
    }
  }

  void _googleSignIn(BuildContext context) async {
    setState(() {
      _isGoogleLoading = true;
    });

    Map response = await AuthApi().googleSignIn();

    if (response['sucess'] == true) {
      setState(() {
        _isGoogleLoading = false;
      });
      print(response);
    } else {
      setState(() {
        _isGoogleLoading = false;
      });
      print(response);
    }

    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text(response['message'])));
  }

  void _facebooklogin(BuildContext context) async {
    setState(() {
      _isFbLoading = true;
    });
    Map response = await AuthApi().facebookSignIn();

    if (response['sucess'] == true) {
      setState(() {
        _isFbLoading = false;
      });
      print(response);
    } else {
      setState(() {
        _isFbLoading = false;
      });
      print(response);
    }

    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text(response['message'])));
  }

  Future<Null> _selectDate(BuildContext context) async {
    int eightTeenYearsAgo = DateTime.now().year - 18;

    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(eightTeenYearsAgo),
        firstDate: DateTime(1920),
        lastDate: DateTime(eightTeenYearsAgo));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dateSelected = true;
      });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey we created above
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Enter your Email'),
            validator: _validateEmail,
            controller: emailController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Enter your name.'),
            validator: _validateName,
            controller: nameController,
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(labelText: 'Enter your Password'),
            validator: _validatePassword,
            controller: passwordController,
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(labelText: 'Confirm your Password'),
            validator: _validateConfirmPassword,
            controller: confirmPassController,
          ),
          SizedBox(
            height: 20.0,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Date of Birth (Optional)',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                  fontSize: 15),
            ),
          ),
          Container(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: GestureDetector(
                onTap: () {
                  _selectDate(context);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(dateSelected == true
                        ? "${selectedDate.toLocal().day}-${selectedDate.toLocal().month}-${selectedDate.toLocal().year}"
                        : 'Please select your date of birth'),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              )),
          SizedBox(height: 20.0),
          MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 2,
            color: Colors.pinkAccent,
            textColor: Colors.white,
            onPressed: _isLoading || _isGoogleLoading || _isFbLoading
                ? null
                : () {
                    _signUp(context);
                  },
            child: Text(_isLoading ? 'Hold on...' : 'Sign Up with Email'),
          ),
          MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 2,
            onPressed: _isLoading || _isGoogleLoading || _isFbLoading
                ? null
                : () {
                    _googleSignIn(context);
                  },
            color: Colors.blueAccent,
            textColor: Colors.white,
            child:
                Text(_isGoogleLoading ? 'Hold on...' : 'Sign Up with Google'),
          ),
          MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 2,
            onPressed: _isLoading || _isGoogleLoading || _isFbLoading
                ? null
                : () {
                    _facebooklogin(context);
                  },
            color: Colors.blue[900],
            textColor: Colors.white,
            child: Text(_isFbLoading ? 'Hold on...' : 'Sign Up with Facebook'),
          )
        ],
      ),
    );
  }
}
