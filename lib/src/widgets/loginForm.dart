import 'package:flutter/material.dart';
import '../services/api/AuthApi.dart';

class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

// Create a corresponding State class. This class will hold the data related to
// the form.
class LoginFormState extends State<LoginForm> {
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a GlobalKey<FormState>, not a GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool _autoValidate = false;

  bool _isLoading = false;
  bool _isGoogleLoading = false;
  bool _isFbLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree

    super.dispose();
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  String _validatePassword(String value) {
    if (value.length > 7) {
      return null;
    }

    return 'Password must be up to 8 characters';
  }

  void _signIn(BuildContext context) async {
    // Validate will return true if the form is valid, or false if
    // the form is invalid.
    setState(() {
      _isLoading = true;
    });
    if (_formKey.currentState.validate()) {
      // If the form is valid, we want to show a Snackbar

      Map param = {
        'email': emailController.text,
        'password': passwordController.text
      };

      final Map response = await AuthApi().signIn(param);

      if (response['sucess'] == true) {
        setState(() {
          _isLoading = false;
        });
        print(response);
      } else {
        setState(() {
          _isLoading = false;
        });
        print(response);
      }
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(response['message'])));
    } else {
      setState(() {
        _autoValidate = true; // activate auto validator after first attempt
        _isLoading = false;
      });
    }
  }

  void _googleSignIn(BuildContext context) async {
    setState(() {
      _isGoogleLoading = true;
    });

    Map response = await AuthApi().googleSignIn();

    if (response['sucess'] == true) {
      setState(() {
        _isGoogleLoading = false;
      });
      print(response);
    } else {
      setState(() {
        _isGoogleLoading = false;
      });
      print(response);
    }

    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text(response['message'])));
  }

  void _facebooklogin(BuildContext context) async {
    setState(() {
      _isFbLoading = true;
    });
    Map response = await AuthApi().facebookSignIn();

    if (response['sucess'] == true) {
      setState(() {
        _isFbLoading = false;
      });
      print(response);
    } else {
      setState(() {
        _isFbLoading = false;
      });
      print(response);
    }

    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text(response['message'])));
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey we created above
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Enter your Email'),
            validator: _validateEmail,
            controller: emailController,
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(labelText: 'Enter your Password'),
            validator: _validatePassword,
            controller: passwordController,
          ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(vertical: 16.0),
          //   child: RaisedButton(
          //     color: Colors.pinkAccent,
          //     textColor: Colors.white,
          //     onPressed: () {
          //       // Validate will return true if the form is valid, or false if
          //       // the form is invalid.
          //       if (_formKey.currentState.validate()) {
          //         // If the form is valid, we want to show a Snackbar
          //         Scaffold.of(context)
          //             .showSnackBar(SnackBar(content: Text('Processing Data')));
          //       } else {
          //         setState(() {
          //           _autoValidate =
          //               true; // activate auto validator after first attempt
          //         });
          //       }
          //     },
          //     child: Text('Login'),
          //   ),
          // ),

          SizedBox(
            height: 30.0,
          ),
          MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 2,
            color: Colors.pinkAccent,
            textColor: Colors.white,
            onPressed: _isLoading || _isGoogleLoading || _isFbLoading
                ? null
                : () {
                    _signIn(context);
                  },
            child: Text(_isLoading ? 'Hold on...' : 'Sign Up with Email'),
          ),
          MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 2,
            onPressed: _isLoading || _isGoogleLoading || _isFbLoading
                ? null
                : () {
                    _googleSignIn(context);
                  },
            color: Colors.blueAccent,
            textColor: Colors.white,
            child:
                Text(_isGoogleLoading ? 'Hold on...' : 'Sign Up with Google'),
          ),
          MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 2,
            onPressed: _isLoading || _isGoogleLoading || _isFbLoading
                ? null
                : () {
                    _facebooklogin(context);
                  },
            color: Colors.blue[900],
            textColor: Colors.white,
            child: Text(_isFbLoading ? 'Hold on...' : 'Sign Up with Facebook'),
          )
        ],
      ),
    );
  }
}
