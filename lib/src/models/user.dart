import 'package:flutter/material.dart';

class UserModel {
  final String userId;
  final String name;
  final String gmId;
  final String fbId;
  final String dateOfBirth;

  final String email;
  final String imageUrl;
  final String token;
  final String createdTime;
  final String updatedTime;

  UserModel(
      {this.userId,
      this.name,
      this.email,
      this.imageUrl,
      this.token,
      this.gmId,
      this.fbId,
      this.dateOfBirth,
      this.createdTime,
      this.updatedTime});

  UserModel.fromMap(Map map)
      : userId = map['userId'],
        token = map['token'],
        name = map['name'],
        email = map['email'],
        fbId = map['fbId'],
        gmId = map['gmId'],
        dateOfBirth = map['dateOfBirth'],
        imageUrl = map['imageUrl'],
        createdTime = map['createdTime'],
        updatedTime = map['updatedTime'];

  Map toJson() => {
        'userId': userId,
        'name': name,
        'email': email,
        'gmId': gmId,
        'fbId': fbId,
        'dateOfBirth': dateOfBirth,
        'token': token,
        'imageUrl': imageUrl,
        'updatedTime': updatedTime,
        'createdTime': createdTime
      };
}
